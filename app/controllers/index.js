/**
 * @file contains entry point of controllers module
 * @author Siti Haryati
 */

const api = require("./api");
const main = require("./main");

module.exports = {
  api,
  main,
};
