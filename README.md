## Directory Structure

```
.
├── app
|   ├── controllers
│       ├── api
│           ├──v1
│               └── cars.js
│               └── index.js
│               └── users.js
│       └── index.js
│       └── main.js
│   └── index.js
│   └── index.j
|  ├── models
│       └── cars.js
│       └── index.js
│       └── users.js
|  ├── repositories
│       └── cars.js
│       └── users.js
|  ├── services
│        └── cars.js
│        └── users.js
|  ├── views
│        └── 404.ejs
│        └── 500.ejs
│        └── index.ejs
|  └── index.js
├── bin
│    └── www.js
├── config
│    └── database.js
│    └── routes.js
│    └── swagger.json
├── db
│    ├── migrations
│           └── 20220510040054-create-cars.js
│           └── 20220510040940-create-users.js
│    ├── seeds
│       └── .gitkeep
├── node_modules
├── public
│    ├── css
│           └── #globals.css
│           └── #huge.css
│    ├── images
│       └── binar.png
|    └── favicon.png
├── .gitignore
├── .sequelizerc
├── .yarnrc
├── package-lock.json
├── package.json
├── README.md
└── yarn.lock
```

## How To Run

```sh
Yarn Start
```

Install 
```sh
yarn install
yarn init -y
yarn add bcryptjs
yarn add cors
yarn add ejs
yarn add express
yarn add jsonwebtoken
yarn add morgan
yarn add pg
yarn add pg-hstore
yarn add sequelize
yarn add requelize-cli
yarn add swagger-ui-express
```

Database

```sh
npx sequelize init
(Sebelum db:create)

  "development": {
    "username": "postgres",
    "password": "password nya",
    "database": "nama db nya",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "test": {
    "username": "postgres",
    "password": "password nya",
    "database": "nama db nya",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "production": {
    "username": "postgres",
    "password": "passwordnys",
    "database": "nama db nya",
    "host": "127.0.0.1",
    "dialect": "postgres"
  }

npx sequelize db:create
yarn migrate
```

Swagger

```
.
├── config
│    └── database.js
│    └── routes.js
│    └── swagger.json
```

run swagger
```
yarn start
    http:localhost/3000/api-docs

Login /api/v1/users/login
│    └── Copy Token
Authorize
│    └── Paste Token
Profil /api/v1/users/profil
│       └── {
              "Name" : "Siti Haryati"
            }
Cars 
Post /api/v1/cars
Get  /api/v1/cars
Put /api/v1/cars/{id}
Delete /api/v1/cars/{id}

Users
Post /apo/v1/users/register
Get /api/V1/users
Get /api/V1/users/profil
Put /api/v1/users/{id}
Delete /api/v1/users/{id}




```

## Endpoint Cras

```
Post /api/v1/cars
Get /api/v1/cars
Put /api/v1/cars/{id}
Get /api/v1/cars/{id}
Delete /api/v1/cars/{id}
```

## Endpoint users & Register

```
Post /api/v1/users/register
Post /api/v1/users/login
Get /api/v1/users
Get /api/v1/users/profil
Put /api/v1/users/{id}
Delete /api/v1/users/{id}
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `yarn db:create` digunakan untuk membuat database
- `yarn db:drop` digunakan untuk menghapus database
- `yarn db:migrate` digunakan untuk menjalankan database migration
- `yarn db:seed` digunakan untuk melakukan seeding
- `yarn db:rollback` digunakan untuk membatalkan migrasi terakhir

